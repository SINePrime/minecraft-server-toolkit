# Summary
- Small set of scripts to make managing modded Minecraft: Java Edition servers easier
- Only supports .zip files of Twitch/ Curse modpacks
	- ie, modpacks which has a manifest.json whose files are on Twitch, and and overrides folder
- Only supports Forge loader (for now)
- Only supports **Ubuntu** machines (for now)

# Purpose
As a child, I payed monthly expenses for overpriced game servers. As a learned software engineer, I prefer to cheaply self-host everything now.

I've found there are simply no alternatives to easily manage modpacks for Minecraft servers.

Hopefully this project should save all of us a pretty penny.

# Dependencies
- https://github.com/portablejim/curseDownloader/

# Usage
To start use using, git clone this repo onto the server machine.

## Setup New Machine
`$ setupMachine.sh`

## Download Modpack
`$ downloadModpack.sh <modpackUrl> <modpackId> <modpackVersion>`
- modpackUrl: 		the URL of the .zip file containing the project.
- modpackId: 		the id of the modpack, without the version
- modpackVersion: 	the version of the modpack. To be clear, this is *not yet automatically detected*.

# Setup Server
`$ setupServer.sh <serverId> <modpackId> <modpackVersion>`

# run the server2
$ screen /usr/bin/java -jar ~/servers/<serverId>/??????