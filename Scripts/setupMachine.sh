#!/bin/bash
# set other scripts executable

# packages
sudo apt update
sudo apt-get install unzip

# sets up the machine for first time use
sudo adduser minecraft

# setup directories
sudo -u minecraft mkdir /home/minecraft/servers
sudo -u minecraft mkdir /home/minecraft/modpacks

# acquire curseDownloader
sudo apt install python3 python3-tk python-pip
sudo -u minecraft git clone https://github.com/portablejim/curseDownloader.git /home/minecraft/curseDownloader
su - minecraft -c 'pip install --user -r /home/minecraft/curseDownloader/requirements.txt'

# set next scripts executable
chmod +x ./Scripts/downloadModpack.sh
chmod +x ./Scripts/setupServer.sh